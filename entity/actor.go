package entity

import (
	"github.com/mirzamk/crm-service/constant"
	"github.com/mirzamk/crm-service/security"
	"gorm.io/gorm"
)

type Actor struct {
	GormModel
	ID         uint
	Username   string
	Password   string
	RoleId     uint
	IsVerified constant.BoolType
	IsActive   constant.BoolType
	Role       *Role
}

func (Actor) TableName() string {
	return "actor"
}

func (a *Actor) BeforeCreate(tx *gorm.DB) (err error) {
	a.Password = security.HashPass(a.Password)
	return
}
