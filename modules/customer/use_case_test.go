package customer

import (
	"context"
	"github.com/mirzamk/crm-service/constant"
	"github.com/mirzamk/crm-service/entity"
	"github.com/mirzamk/crm-service/payload"
	"github.com/mirzamk/crm-service/utils/helper"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"testing"
)

type MockActorRepo struct {
	mock.Mock
}

func (m *MockActorRepo) GetActorByName(username string) (entity.Actor, error) {
	args := m.Called(username)
	return args.Get(0).(entity.Actor), args.Error(1)
}

func (m *MockActorRepo) GetRole(roleName string) (entity.Role, error) {
	args := m.Called(roleName)
	return args.Get(0).(entity.Role), args.Error(1)
}

func (m *MockActorRepo) CreateActor(actor *entity.Actor) error {
	args := m.Called(actor)
	return args.Error(0)
}

func (m *MockActorRepo) GetActorById(id uint) (entity.Actor, error) {
	args := m.Called(id)
	return args.Get(0).(entity.Actor), args.Error(1)
}

func (m *MockActorRepo) SearchActorByName(pagination *helper.Pagination, name string) (*helper.Pagination, error) {
	args := m.Called(pagination, name)
	return args.Get(0).(*helper.Pagination), args.Error(1)
}

func (m *MockActorRepo) GetAllActor(pagination *helper.Pagination) (*helper.Pagination, error) {
	args := m.Called(pagination)
	return args.Get(0).(*helper.Pagination), args.Error(1)
}

func (m *MockActorRepo) CountRowActor(totalRows *int64) error {
	args := m.Called(totalRows)
	*totalRows = args.Get(0).(int64)
	return args.Error(1)
}

func (m *MockActorRepo) UpdateActor(actor entity.Actor, id uint) error {
	args := m.Called(actor, id)
	return args.Error(0)
}

func (m *MockActorRepo) DeleteActor(id uint) error {
	args := m.Called(id)
	return args.Error(0)
}

type MockCustomerRepo struct {
	mock.Mock
}

func (m *MockCustomerRepo) GetCustomerById(id uint) (entity.Customer, error) {
	args := m.Called(id)
	return args.Get(0).(entity.Customer), args.Error(1)
}

func (m *MockCustomerRepo) GetAllCustomer(pagination helper.Pagination) (*helper.Pagination, error) {
	args := m.Called(pagination)
	return args.Get(0).(*helper.Pagination), args.Error(1)
}

func (m *MockCustomerRepo) CountRowCustomer(totalRows *int64) error {
	args := m.Called(totalRows)
	return args.Error(0)
}

func (m *MockCustomerRepo) SearchCustomerByName(pagination helper.Pagination, name string) (*helper.Pagination, error) {
	args := m.Called(pagination, name)
	return args.Get(0).(*helper.Pagination), args.Error(1)
}

func (m *MockCustomerRepo) SearchCustomerByEmail(pagination helper.Pagination, email string) (*helper.Pagination, error) {
	args := m.Called(pagination, email)
	return args.Get(0).(*helper.Pagination), args.Error(1)
}

func (m *MockCustomerRepo) SearchCustomerByNameOrEmail(pagination helper.Pagination, name string, email string) (*helper.Pagination, error) {
	args := m.Called(pagination, name, email)
	return args.Get(0).(*helper.Pagination), args.Error(1)
}

func (m *MockCustomerRepo) CreateCustomer(customer entity.Customer) error {
	args := m.Called(customer)
	return args.Error(0)
}

func (m *MockCustomerRepo) UpdateCustomer(customer entity.Customer, id uint) error {
	args := m.Called(customer, id)
	return args.Error(0)
}

func (m *MockCustomerRepo) DeleteCustomer(id uint) error {
	args := m.Called(id)
	return args.Error(0)
}

func TestCreateCustomer(t *testing.T) {
	actorRepo := new(MockActorRepo)
	customerRepo := new(MockCustomerRepo)
	useCase := &useCaseCustomer{
		ActorRepo:    actorRepo,
		CustomerRepo: customerRepo,
	}

	ctx := context.WithValue(context.Background(), "adminId", uint(123))
	adminID := uint(123)
	customer := CustomerDto{
		Firstname: "John",
		Lastname:  "Doe",
		Email:     "john.doe@example.com",
		Avatar:    "avatar.jpg",
	}

	admin := entity.Actor{
		ID:         adminID,
		IsActive:   constant.True,
		IsVerified: constant.True,
	}

	actorRepo.On("GetActorById", adminID).Return(admin, nil)
	customerRepo.On("CreateCustomer", mock.AnythingOfType("entity.Customer")).Return(nil)

	err := useCase.CreateCustomer(ctx, customer)

	assert.NoError(t, err)

	actorRepo.AssertCalled(t, "GetActorById", adminID)
	customerRepo.AssertCalled(t, "CreateCustomer", mock.AnythingOfType("entity.Customer"))
}

func TestGetCustomerById(t *testing.T) {
	actorRepo := new(MockActorRepo)
	customerRepo := new(MockCustomerRepo)
	useCase := &useCaseCustomer{
		ActorRepo:    actorRepo,
		CustomerRepo: customerRepo,
	}

	adminID := uint(123)
	ctx := context.WithValue(context.Background(), "adminId", adminID)
	admin := entity.Actor{
		ID:       adminID,
		Username: "admin",
		IsActive: constant.True,
	}
	customerID := uint(456)
	customer := entity.Customer{
		ID:        customerID,
		Firstname: "John",
		Lastname:  "Doe",
		Email:     "john.doe@example.com",
		Avatar:    "avatar.jpg",
	}
	actorRepo.On("GetActorById", adminID).Return(admin, nil)
	customerRepo.On("GetCustomerById", customerID).Return(customer, nil)

	result, err := useCase.GetCustomerById(ctx, int(customerID))
	assert.NoError(t, err)

	expected := CustomerDto{
		Firstname: customer.Firstname,
		Lastname:  customer.Lastname,
		Email:     customer.Email,
		Avatar:    customer.Avatar,
	}
	assert.Equal(t, expected, result)

	actorRepo.AssertExpectations(t)
	customerRepo.AssertExpectations(t)
}

func TestSearchCustomer(t *testing.T) {
	// Create a mock ActorRepo and CustomerRepo
	actorRepo := new(MockActorRepo)
	customerRepo := new(MockCustomerRepo)
	useCase := &useCaseCustomer{
		ActorRepo:    actorRepo,
		CustomerRepo: customerRepo,
	}

	// Set up the mock dependencies and expected results
	ctx := context.WithValue(context.Background(), "adminId", uint(1))
	actor := &entity.Actor{
		ID:       1,
		IsActive: constant.True,
	}
	actorRepo.On("GetActorById", uint(1)).Return(actor, nil)

	filter := map[string]string{
		"page":    "1",
		"limit":   "10",
		"sortby":  "lastname",
		"orderby": "asc",
		"name":    "John",
	}

	pagination := &helper.Pagination{
		Limit:     10,
		Page:      1,
		Sort:      "lastname asc",
		TotalRows: 2,
		Rows: []*entity.Customer{
			{
				Firstname: "John",
				Lastname:  "Doe",
				Email:     "john.doe@example.com",
				Avatar:    "avatar.jpg",
			},
			{
				Firstname: "John",
				Lastname:  "Smith",
				Email:     "john.smith@example.com",
				Avatar:    "avatar.jpg",
			},
		},
	}
	customerRepo.On("SearchCustomerByName", pagination, "John").Return(pagination, nil)

	// Call the function under test
	result, err := useCase.SearchCustomer(ctx, filter)

	// Assertions
	assert.NoError(t, err)
	assert.Equal(t, pagination, result)
	actorRepo.AssertExpectations(t)
	customerRepo.AssertExpectations(t)
}

func TestUpdateCustomer(t *testing.T) {
	actorRepo := new(MockActorRepo)
	customerRepo := new(MockCustomerRepo)
	useCase := &useCaseCustomer{
		ActorRepo:    actorRepo,
		CustomerRepo: customerRepo,
	}

	ctx := context.WithValue(context.Background(), "adminId", uint(1))

	// Mock the GetActorById function
	actor := &entity.Actor{
		ID:       1,
		IsActive: constant.True,
	}
	actorRepo.On("GetActorById", uint(1)).Return(actor, nil)

	// Mock the GetCustomerById function
	customer := &entity.Customer{
		ID:        1,
		Firstname: "John",
		Lastname:  "Doe",
		Email:     "john.doe@example.com",
		Avatar:    "avatar.jpg",
	}
	customerRepo.On("GetCustomerById", uint(1)).Return(customer, nil)

	// Mock the UpdateCustomer function
	updatedCustomer := entity.Customer{
		ID:        1,
		Firstname: "John",
		Lastname:  "Smith",
		Email:     "john.smith@example.com",
		Avatar:    "new_avatar.jpg",
	}
	customerRepo.On("UpdateCustomer", updatedCustomer, uint(1)).Return(nil)

	// Define the payload for updating the customer
	updatePayload := payload.UpdateCustomer{
		Firstname: "John",
		Lastname:  "Smith",
		Email:     "john.smith@example.com",
		Avatar:    "new_avatar.jpg",
	}

	err := useCase.UpdateCustomer(ctx, updatePayload, 1)

	assert.NoError(t, err)
	actorRepo.AssertExpectations(t)
	customerRepo.AssertExpectations(t)
}

func TestDeleteCustomer(t *testing.T) {
	actorRepo := new(MockActorRepo)
	customerRepo := new(MockCustomerRepo)
	useCase := &useCaseCustomer{
		ActorRepo:    actorRepo,
		CustomerRepo: customerRepo,
	}

	ctx := context.WithValue(context.Background(), "adminId", uint(1))

	// Mock the GetActorById function
	actor := &entity.Actor{
		ID:       1,
		IsActive: constant.True,
	}
	actorRepo.On("GetActorById", uint(1)).Return(actor, nil)

	// Mock the GetCustomerById function
	customer := &entity.Customer{
		ID:        1,
		Firstname: "John",
		Lastname:  "Doe",
		Email:     "john.doe@example.com",
		Avatar:    "avatar.jpg",
	}
	customerRepo.On("GetCustomerById", uint(1)).Return(customer, nil)

	// Mock the DeleteCustomer function
	customerRepo.On("DeleteCustomer", uint(1)).Return(nil)

	err := useCase.DeleteCustomer(ctx, 1)

	assert.NoError(t, err)
	actorRepo.AssertExpectations(t)
	customerRepo.AssertExpectations(t)
}
