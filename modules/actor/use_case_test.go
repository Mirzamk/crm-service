package actor

import (
	"github.com/mirzamk/crm-service/config"
	"github.com/mirzamk/crm-service/constant"
	"github.com/mirzamk/crm-service/entity"
	"github.com/mirzamk/crm-service/payload"
	"github.com/mirzamk/crm-service/repository/mocks"
	"github.com/mirzamk/crm-service/security"
	"github.com/mirzamk/crm-service/utils/helper"
	"github.com/stretchr/testify/require"
	"testing"
)

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type MockActorRepo struct {
	mock.Mock
}

func (m *MockActorRepo) GetActorByName(username string) (entity.Actor, error) {
	args := m.Called(username)
	return args.Get(0).(entity.Actor), args.Error(1)
}

func (m *MockActorRepo) GetRole(roleName string) (entity.Role, error) {
	args := m.Called(roleName)
	return args.Get(0).(entity.Role), args.Error(1)
}

func (m *MockActorRepo) CreateActor(actor *entity.Actor) error {
	args := m.Called(actor)
	return args.Error(0)
}

func (m *MockActorRepo) GetActorById(id uint) (entity.Actor, error) {
	args := m.Called(id)
	return args.Get(0).(entity.Actor), args.Error(1)
}

func (m *MockActorRepo) SearchActorByName(pagination *helper.Pagination, name string) (*helper.Pagination, error) {
	args := m.Called(pagination, name)
	return args.Get(0).(*helper.Pagination), args.Error(1)
}

func (m *MockActorRepo) GetAllActor(pagination *helper.Pagination) (*helper.Pagination, error) {
	args := m.Called(pagination)
	return args.Get(0).(*helper.Pagination), args.Error(1)
}

func (m *MockActorRepo) CountRowActor(totalRows *int64) error {
	args := m.Called(totalRows)
	*totalRows = args.Get(0).(int64)
	return args.Error(1)
}

func (m *MockActorRepo) UpdateActor(actor entity.Actor, id uint) error {
	args := m.Called(actor, id)
	return args.Error(0)
}

func (m *MockActorRepo) DeleteActor(id uint) error {
	args := m.Called(id)
	return args.Error(0)
}

type MockApprovalRepo struct {
	mock.Mock
}

func (m *MockApprovalRepo) SearchApproval() ([]entity.Approval, error) {
	args := m.Called()
	return args.Get(0).([]entity.Approval), args.Error(1)
}

func (m *MockApprovalRepo) SearchApprovalByStatus(status string) ([]entity.Approval, error) {
	args := m.Called(status)
	return args.Get(0).([]entity.Approval), args.Error(1)
}

func (m *MockApprovalRepo) GetApprovalById(id uint) (entity.Approval, error) {
	args := m.Called(id)
	return args.Get(0).(entity.Approval), args.Error(1)
}

func (m *MockApprovalRepo) GetApprovalByActorId(id uint) (entity.Approval, error) {
	args := m.Called(id)
	return args.Get(0).(entity.Approval), args.Error(1)
}

func (m *MockApprovalRepo) UpdateApproval(approval entity.Approval, id uint) error {
	args := m.Called(approval, id)
	return args.Error(0)
}

func (m *MockApprovalRepo) CreateApproval(approval entity.Approval) error {
	args := m.Called(approval)
	return args.Error(0)
}

func TestRegister_Success(t *testing.T) {

	actorRepo := new(MockActorRepo)
	approvalRepo := new(MockApprovalRepo)
	useCase := &useCaseActor{
		ActorRepo:    actorRepo,
		ApprovalRepo: approvalRepo,
	}

	existingActor := entity.Actor{Username: "existing_user"}
	actorRepo.On("GetActorByName", existingActor.Username).Return(existingActor, nil)
	actorRepo.On("GetRole", entity.ROLE_ADMIN).Return(entity.Role{}, nil)
	actorRepo.On("CreateActor", mock.AnythingOfType("*entity.Actor")).Return(nil)
	actorRepo.On("GetActorByName", config.Config.SuperAccount.SuperName).Return(entity.Actor{}, nil)
	approvalRepo.On("CreateApproval", mock.AnythingOfType("entity.Approval")).Return(nil)

	actor := payload.AuthActor{
		Username: "new_user",
		Password: "password",
	}

	err := useCase.Register(actor)

	assert.NoError(t, err)
	actorRepo.AssertExpectations(t)
	approvalRepo.AssertExpectations(t)
}

func TestRegister_ExistingUsername(t *testing.T) {

	actorRepo := new(MockActorRepo)
	approvalRepo := new(MockApprovalRepo)
	useCase := &useCaseActor{
		ActorRepo:    actorRepo,
		ApprovalRepo: approvalRepo,
	}

	existingActor := entity.Actor{Username: "existing_user"}
	actorRepo.On("GetActorByName", existingActor.Username).Return(existingActor, nil)

	actor := payload.AuthActor{
		Username: existingActor.Username,
		Password: "password",
	}

	err := useCase.Register(actor)

	assert.EqualError(t, err, constant.ErrAdminUsernameExists.Error())
	actorRepo.AssertExpectations(t)
	approvalRepo.AssertExpectations(t)
}

func TestRegister_RoleNotFound(t *testing.T) {

	actorRepo := new(MockActorRepo)
	approvalRepo := new(MockApprovalRepo)
	useCase := &useCaseActor{
		ActorRepo:    actorRepo,
		ApprovalRepo: approvalRepo,
	}

	actorRepo.On("GetActorByName", mock.Anything).Return(entity.Actor{}, nil)
	actorRepo.On("GetRole", entity.ROLE_ADMIN).Return(entity.Role{}, errors.New("role not found"))

	actor := payload.AuthActor{
		Username: "new_user",
		Password: "password",
	}

	err := useCase.Register(actor)

	assert.EqualError(t, err, constant.ErrRoleNotFound.Error())
	actorRepo.AssertExpectations(t)
	approvalRepo.AssertExpectations(t)
}

func TestRegister_ActorCreationError(t *testing.T) {

	actorRepo := new(MockActorRepo)
	approvalRepo := new(MockApprovalRepo)
	useCase := &useCaseActor{
		ActorRepo:    actorRepo,
		ApprovalRepo: approvalRepo,
	}

	actorRepo.On("GetActorByName", mock.Anything).Return(entity.Actor{}, nil)
	actorRepo.On("GetRole", entity.ROLE_ADMIN).Return(entity.Role{}, nil)
	actorRepo.On("CreateActor", mock.AnythingOfType("*entity.Actor")).Return(errors.New("actor creation failed"))

	actor := payload.AuthActor{
		Username: "new_user",
		Password: "password",
	}

	err := useCase.Register(actor)

	assert.EqualError(t, err, "actor creation failed")
	actorRepo.AssertExpectations(t)
	approvalRepo.AssertExpectations(t)
}
func TestGetActorById_Success(t *testing.T) {

	actorRepo := new(MockActorRepo)
	useCase := &useCaseActor{
		ActorRepo: actorRepo,
	}

	expectedActor := entity.Actor{
		Username:   "test_user",
		IsVerified: constant.True,
		IsActive:   constant.True,
		Role:       &entity.Role{Rolename: "admin"},
	}

	actorRepo.On("GetActorById", mock.Anything).Return(expectedActor, nil)

	actorID := 123
	actor, err := useCase.GetActorById(actorID)

	assert.NoError(t, err)
	assert.Equal(t, "test_user", actor.Username)
	assert.Equal(t, "True", actor.IsVerified)
	assert.Equal(t, "True", actor.IsActive)
	assert.Equal(t, "admin", actor.Role)
	actorRepo.AssertExpectations(t)
}

func TestGetActorById_Error(t *testing.T) {

	actorRepo := new(MockActorRepo)
	useCase := &useCaseActor{
		ActorRepo: actorRepo,
	}

	expectedError := constant.ErrAdminNotFound

	actorRepo.On("GetActorById", mock.Anything).Return(entity.Actor{}, expectedError)

	actorID := 123
	actor, err := useCase.GetActorById(actorID)

	assert.Equal(t, ActorDto{}, actor)
	assert.Equal(t, expectedError, err)
	actorRepo.AssertExpectations(t)
}
func TestSearchActorByName_WithName(t *testing.T) {

	actorRepo := new(MockActorRepo)
	useCase := &useCaseActor{
		ActorRepo: actorRepo,
	}

	filter := map[string]string{
		"page":    "1",
		"limit":   "10",
		"sortby":  "username",
		"orderby": "asc",
		"name":    "test_name",
	}

	expectedPagination := &helper.Pagination{
		Limit:     10,
		Page:      1,
		Sort:      "username asc",
		TotalRows: 20,
		Rows: []*entity.Actor{
			{
				Username:   "test_name",
				IsVerified: constant.True,
				IsActive:   constant.True,
				Role:       &entity.Role{Rolename: "admin"},
			},
		},
	}

	var totalRows int64 = 20
	actorRepo.On("CountRowActor", mock.AnythingOfType("*int64")).Return(totalRows, nil)

	actorRepo.On("SearchActorByName", mock.AnythingOfType("*helper.Pagination"), "test_name").
		Return(expectedPagination, nil)

	result, err := useCase.SearchActorByName(filter)

	assert.NoError(t, err)
	assert.Equal(t, expectedPagination, result)
	actorRepo.AssertExpectations(t)
}

func TestSearchActorByName_WithoutName(t *testing.T) {

	actorRepo := new(MockActorRepo)
	useCase := &useCaseActor{
		ActorRepo: actorRepo,
	}

	filter := map[string]string{
		"page":    "1",
		"limit":   "10",
		"sortby":  "username",
		"orderby": "asc",
	}

	expectedPagination := &helper.Pagination{
		Limit:     10,
		Page:      1,
		Sort:      "username asc",
		TotalRows: 20,
		Rows: []*entity.Actor{
			{
				Username:   "user1",
				IsVerified: constant.True,
				IsActive:   constant.True,
				Role:       &entity.Role{Rolename: "admin"},
			},
			{
				Username:   "user2",
				IsVerified: constant.True,
				IsActive:   constant.True,
				Role:       &entity.Role{Rolename: "admin"},
			},
		},
	}

	var totalRows int64 = 20
	actorRepo.On("CountRowActor", mock.AnythingOfType("*int64")).Return(totalRows, nil)

	actorRepo.On("GetAllActor", mock.AnythingOfType("*helper.Pagination")).
		Return(expectedPagination, nil)

	result, err := useCase.SearchActorByName(filter)

	assert.NoError(t, err)
	assert.Equal(t, expectedPagination, result)
	actorRepo.AssertExpectations(t)
}

func TestUpdateActor_Success(t *testing.T) {

	actorRepo := new(MockActorRepo)
	useCase := &useCaseActor{
		ActorRepo: actorRepo,
	}

	updateActor := payload.UpdateActor{
		Username: "new_username",
		Password: "new_password",
	}
	actorID := 123

	expectedActor := entity.Actor{
		Username: "existing_username",
		Password: "existing_password",
	}
	actorRepo.On("GetActorById", uint(actorID)).Return(expectedActor, nil)

	actorUpdate := entity.Actor{
		Username: "new_username",
		Password: security.HashPass(updateActor.Password),
	}
	actorRepo.On("UpdateActor", actorUpdate, uint(actorID)).Return(nil)

	err := useCase.UpdateActor(updateActor, actorID)

	assert.NoError(t, err)
	actorRepo.AssertExpectations(t)
}

func TestUpdateActor_UpdateError(t *testing.T) {

	actorRepo := new(MockActorRepo)
	useCase := &useCaseActor{
		ActorRepo: actorRepo,
	}

	updateActor := payload.UpdateActor{
		Username: "new_username",
		Password: "new_password",
	}
	actorID := 123

	expectedActor := entity.Actor{
		Username: "existing_username",
		Password: "existing_password",
	}
	actorRepo.On("GetActorById", uint(actorID)).Return(expectedActor, nil)

	actorUpdate := entity.Actor{
		Username: "new_username",
		Password: "hashed_new_password",
	}
	actorRepo.On("UpdateActor", actorUpdate, uint(actorID)).Return(errors.New("update error"))

	err := useCase.UpdateActor(updateActor, actorID)

	assert.EqualError(t, err, "update error")
	actorRepo.AssertExpectations(t)
}

func TestDeleteActor_Success(t *testing.T) {

	actorRepo := new(MockActorRepo)
	useCase := &useCaseActor{
		ActorRepo: actorRepo,
	}

	actorID := 123

	expectedActor := entity.Actor{
		Username: "existing_username",
		Password: "existing_password",
	}
	actorRepo.On("GetActorById", uint(actorID)).Return(expectedActor, nil)

	actorRepo.On("DeleteActor", uint(actorID)).Return(nil)

	err := useCase.DeleteActor(actorID)

	assert.NoError(t, err)
	actorRepo.AssertExpectations(t)
}

func TestDeleteActor_ActorNotFound(t *testing.T) {

	actorRepo := new(MockActorRepo)
	useCase := &useCaseActor{
		ActorRepo: actorRepo,
	}

	actorID := 123

	actorRepo.On("GetActorById", uint(actorID)).Return(entity.Actor{}, constant.ErrAdminNotFound)

	err := useCase.DeleteActor(actorID)

	assert.EqualError(t, err, constant.ErrAdminNotFound.Error())
	actorRepo.AssertExpectations(t)
}

func TestUpdateFlagActor(t *testing.T) {

	actorRepo := new(MockActorRepo)
	approvalRepo := new(MockApprovalRepo)
	useCase := &useCaseActor{
		ActorRepo:    actorRepo,
		ApprovalRepo: approvalRepo,
	}

	actorID := 123
	actor := ActorDto{
		IsActive:   "true",
		IsVerified: "true",
	}

	actorRepo.On("GetActorById", uint(actorID)).Return(entity.Actor{ID: uint(actorID)}, nil)

	approvalRepo.On("GetApprovalByActorId", uint(actorID)).Return(entity.Approval{Status: "approve"}, nil)

	actorRepo.On("UpdateActor", mock.AnythingOfType("entity.Actor"), uint(actorID)).Return(nil)

	err := useCase.UpdateFlagActor(actor, actorID)

	assert.NoError(t, err)
	actorRepo.AssertExpectations(t)
	approvalRepo.AssertExpectations(t)
}

func TestSearchApproval(t *testing.T) {
	approvalRepo := new(MockApprovalRepo)
	useCase := &useCaseActor{
		ApprovalRepo: approvalRepo,
	}

	expectedApprovals := []entity.Approval{
		{
			ID: 123,
			Admin: &entity.Actor{
				Username:   "admin",
				IsVerified: constant.True,
				IsActive:   constant.True,
			},
		},
	}

	approvalRepo.On("SearchApproval").Return(expectedApprovals, nil)

	approvals, err := useCase.SearchApproval()

	assert.NoError(t, err)
	assert.Len(t, approvals, len(expectedApprovals))

	approvalRepo.AssertExpectations(t)
}

func TestSearchApprovalByStatus(t *testing.T) {
	mockApprovalRepo := &mocks.ApprovalInterfaceRepository{}
	au := useCaseActor{ApprovalRepo: mockApprovalRepo}

	expectedApprovals := []entity.Approval{
		{
			ID: 123,
			Admin: &entity.Actor{
				Username:   "admin",
				IsVerified: constant.True,
				IsActive:   constant.True,
			},
			Status: "approved",
		},
	}

	mockApprovalRepo.On("SearchApprovalByStatus", "approved").Return(expectedApprovals, nil)

	expectedDTOs := []ApprovalDto{
		{
			ID: 123,
			Admin: ActorDto{
				Username:   "admin",
				IsVerified: "True",
				IsActive:   "True",
			},
			Status: "approved",
		},
	}

	result, err := au.SearchApprovalByStatus("approved")
	require.NoError(t, err)
	require.Equal(t, expectedDTOs, result)

	mockApprovalRepo.AssertCalled(t, "SearchApprovalByStatus", "approved")
}

func TestGetApprovalById(t *testing.T) {
	approvalRepo := new(MockApprovalRepo)
	useCase := &useCaseActor{
		ApprovalRepo: approvalRepo,
	}

	approvalID := 123
	expectedApproval := entity.Approval{
		ID: uint(approvalID),
		Admin: &entity.Actor{
			Username:   "admin",
			IsVerified: constant.True,
			IsActive:   constant.True,
		},
		Status: "approved",
	}

	approvalRepo.On("GetApprovalById", uint(approvalID)).Return(expectedApproval, nil)

	approvalDto, err := useCase.GetApprovalById(approvalID)

	assert.NoError(t, err)
	assert.Equal(t, expectedApproval.ID, approvalDto.ID)
	assert.Equal(t, expectedApproval.Admin.Username, approvalDto.Admin.Username)
	assert.Equal(t, string(expectedApproval.Admin.IsVerified), approvalDto.Admin.IsVerified)
	assert.Equal(t, string(expectedApproval.Admin.IsActive), approvalDto.Admin.IsActive)
	assert.Equal(t, expectedApproval.Status, approvalDto.Status)

	approvalRepo.AssertExpectations(t)
}

func TestChangeStatusApproval(t *testing.T) {
	approvalRepo := new(MockApprovalRepo)
	useCase := &useCaseActor{
		ApprovalRepo: approvalRepo,
	}

	approvalID := 123
	status := payload.ApprovalStatus{
		Status: "approved",
	}

	approvalRepo.On("GetApprovalById", uint(approvalID)).Return(entity.Approval{}, nil)
	approvalRepo.On("UpdateApproval", mock.AnythingOfType("entity.Approval"), uint(approvalID)).Return(nil)

	err := useCase.ChangeStatusApproval(approvalID, status)

	assert.NoError(t, err)

	approvalRepo.AssertCalled(t, "GetApprovalById", uint(approvalID))
	approvalRepo.AssertCalled(t, "UpdateApproval", mock.AnythingOfType("entity.Approval"), uint(approvalID))
}
